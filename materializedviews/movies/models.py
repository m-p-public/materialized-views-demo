from django.db import models

# Create your models here.
from django.db import models
from django.db.models import Avg
from django_materialized_view.base_model import MaterializedViewModel
from django.contrib import admin


class Movie(models.Model):
    imdb_id = models.CharField(max_length=20, unique=True)
    title = models.CharField(max_length=500)
    year = models.IntegerField(null=True, blank=True)
    genres = models.CharField(max_length=200, null=True, blank=True)
    runtime_minutes = models.IntegerField(null=True, blank=True)

    class Meta:
        indexes = [
            models.Index(fields=['year']),
            models.Index(fields=['genres']),
        ]

class YearlyRuntimeModel(MaterializedViewModel):
    create_pkey_index = True  # if you need add unique field as a primary key and create indexes

    class Meta:
        managed = False

    # if create_pkey_index=True you must add argument primary_key=True
    year = models.IntegerField(primary_key=True)
    average_runtime = models.IntegerField()

    @staticmethod
    def get_query_from_queryset():
        # define this method only in case use queryset as a query for materialized view.
        # Method must return Queryset
        return Movie.objects.values('year').annotate(average_runtime=Avg('runtime_minutes'))
