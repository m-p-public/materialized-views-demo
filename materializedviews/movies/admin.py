from django.contrib import admin
from .models import Movie, YearlyRuntimeModel

admin.site.register(Movie)


@admin.register(YearlyRuntimeModel)
class YearlyRuntimeModelAdmin(admin.ModelAdmin):
    readonly_fields = ['year', 'average_runtime']
    list_display = ['year', 'average_runtime']
    ordering = ['year']
    list_display_links = None

    # format the column "average runtime" 
