from django.shortcuts import render
from movies.models import YearlyRuntimeModel

def yearly_runtime_graph(request):
    # Get the data from the YearlyRuntimeModel
    yearly_runtimes = YearlyRuntimeModel.objects.all()

    # Prepare the data for the chart
    data = {
        'labels': [str(y.year) for y in yearly_runtimes],
        'values': [float(y.average_runtime) if y.average_runtime is not None else 0 for y in yearly_runtimes],
    }

    # Render the chart template with the data
    return render(request, 'yearly_runtime_graph.phtml', {'data': data})
