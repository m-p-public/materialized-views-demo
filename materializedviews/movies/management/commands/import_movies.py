import csv
from django.core.management.base import BaseCommand
from movies.models import Movie


class Command(BaseCommand):
    help = 'Import movies from title.basics.tsv'

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str, help='Path to title.basics.tsv file')

    def handle(self, *args, **options):
        file_path = options['file'] or 'title.basics.tsv'
        movies = []
        with open(file_path, encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile, delimiter='\t')
            for row in reader:
                try:
                    if row['titleType'] != 'movie':
                        continue
                    imdb_id = row['tconst']
                    title = row['originalTitle']
                    year = int(row['startYear']) if row['startYear'] != '\\N' else None
                    genres = row['genres'] if row['genres'] != '\\N' else None
                    runtime_minutes = int(row['runtimeMinutes']) if row['runtimeMinutes'] != '\\N' else None
                    movie = Movie(imdb_id=imdb_id, title=title, year=year, genres=genres, runtime_minutes=runtime_minutes)
                    movies.append(movie)
                except ValueError:
                    # Skip movies with invalid year or runtime
                    continue
        Movie.objects.bulk_create(movies)
        self.stdout.write(self.style.SUCCESS(f'Successfully imported {len(movies)} movies'))
